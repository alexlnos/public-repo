package job

import (
	"log"
	"panda-crm/entity"
	"panda-crm/repository"
	"time"
)

func UpdateShippingInfo() {
	for {
		list, err := repository.GetRussianPostOrders()
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
			continue
		}

		for _, order := range list {
			var shippingInfo []entity.ShippingInfo

			trackInfo, err := repository.GetRussianPostTrackInfo(order.TrackCode)
			if err != nil {
				log.Println(err)
				time.Sleep(5 * time.Second)
				continue
			}

			if len(trackInfo.List) == 0 {
				continue
			}

			trackList := trackInfo.List[0].TrackingItem.TrackingHistoryItemList
			for i := len(trackList) - 1; i >= 0; i-- {
				if trackList[i].OperationType == 28 && trackList[i].OperationAttr == 0 {
					// Не отправлен
					shippingInfo = append(shippingInfo, entity.ShippingInfo{OrderId: order.Id, Date: trackList[i].Date, Status: entity.ShippingInfoNotSent})
				} else if trackList[i].OperationType == 1 && trackList[i].OperationAttr == 1 {
					// Отправлен
					shippingInfo = append(shippingInfo, entity.ShippingInfo{OrderId: order.Id, Date: trackList[i].Date, Status: entity.ShippingInfoSent})
				} else if trackList[i].OperationType == 8 && trackList[i].OperationAttr == 2 {
					// Ожидаем выкупа
					shippingInfo = append(shippingInfo, entity.ShippingInfo{OrderId: order.Id, Date: trackList[i].Date, Status: entity.ShippingWaitRecipient})
				} else if trackList[i].OperationType == 2 && trackList[i].OperationAttr == 1 {
					// Получено адресатом
					shippingInfo = append(shippingInfo, entity.ShippingInfo{OrderId: order.Id, Date: trackList[i].Date, Status: entity.ShippingReceived})
					break
				} else if trackList[i].OperationType == 3 && trackList[i].OperationAttr == 0 {
					// Выслано обратно отправителю
					shippingInfo = append(shippingInfo, entity.ShippingInfo{OrderId: order.Id, Date: trackList[i].Date, Status: entity.ShippingSentSender})
					break
				}
			}

			err = repository.SaveShippingInfo(shippingInfo)
			if err != nil {
				log.Println(err)
				time.Sleep(5 * time.Second)
				continue
			}
		}

		time.Sleep(5 * time.Minute)
	}
}
