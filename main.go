package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"os"
	"panda-crm/entity"
	"panda-crm/job"
	"panda-crm/middleware"
)

func main() {
	gin.SetMode(gin.ReleaseMode)

	go job.UpdateShippingInfo()

	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(cors.Default())

	v1 := router.Group("/v1", middleware.ResponseWriter)
	{
		// Auth
		v1.GET("/auth",
			middleware.BindEntity(&entity.Auth{}),
			middleware.Authorization,
		)

		withToken := v1.Group("", middleware.VerifyToken)
		{
			// List
			withToken.GET("/order-type-list", middleware.OrderTypeList)
			withToken.GET("/messenger-list", middleware.MessengerList)
			withToken.GET("/delivery-list", middleware.DeliveryList)

			withToken.GET("/project-list", middleware.ProjectList)
			withToken.GET("/product-list",
				middleware.BindEntity(&entity.ProductFilter{}),
				middleware.ProductList,
			)

			// Order
			withToken.GET("/order-list",
				middleware.BindEntity(&entity.OrderListFilter{}),
				middleware.OrderList,
			)

			withToken.GET("/order/blank",
				middleware.BindEntity(&entity.OrderBlankPayload{}),
				middleware.DownloadOrderBlank,
			)

			withToken.PUT("/order",
				middleware.BindEntity(&entity.Order{}),
				middleware.OrderUpdate,
			)

			withToken.POST("/order",
				middleware.BindEntity(&entity.OrderPayload{}),
				middleware.OrderValidation,
				middleware.ClientCreate,
				middleware.ClientMessengerCreate,
				middleware.OrderCreate,
				middleware.OrderProductCreate,
			)
		}
	}

	router.Run(os.ExpandEnv(":$HTTP_PORT"))
}
