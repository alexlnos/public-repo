package entity

type Client struct {
	Id         int               `json:"id" gorm:"column:id"`
	FullName   string            `json:"full_name" gorm:"column:full_name"`
	Address    string            `json:"address" gorm:"column:address"`
	Phone      string            `json:"phone" gorm:"column:phone"`
	Messengers []ClientMessenger `json:"messengers" gorm:"-"`
}
