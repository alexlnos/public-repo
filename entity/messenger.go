package entity

type ClientMessenger struct {
	ClientId int    `json:"-" gorm:"column:client_id"`
	Type     int    `json:"type" gorm:"column:type"`
	Value    string `json:"value" gorm:"column:value"`
}
