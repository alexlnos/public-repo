package entity

import "time"

type RussianPostBlank struct {
	Path      string
	TrackCode string
}

type OrderBlankPayload struct {
	Id []int `form:"id"`
}

type RussianPostTrackPayload struct {
	List []struct {
		TrackingItem struct {
			TrackingHistoryItemList []struct {
				Date          time.Time `json:"date"`
				OperationType int       `json:"operationType"`
				OperationAttr int       `json:"operationAttr"`
			} `json:"trackingHistoryItemList"`
		} `json:"trackingItem"`
	} `json:"list"`
}
