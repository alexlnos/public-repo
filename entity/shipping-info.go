package entity

import "time"

const (
	ShippingInfoNotSent = iota + 1
	ShippingInfoSent
	ShippingWaitRecipient
	ShippingReceived
	ShippingSentSender
)

type ShippingInfo struct {
	Id      int       `json:"-"`
	Date    time.Time `json:"date"`
	OrderId int       `json:"-"`
	Status  int       `json:"status"`
}
