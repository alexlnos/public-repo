package entity

const (
	UserPrivilegeAdmin = iota + 1
	UserPrivilegeManager
)

type (
	User struct {
		Id           int    `json:"id"`
		Name         string `json:"name"`
		Login        string `json:"login"`
		PasswordHash string `json:"-"`
		Token        string `json:"token"`
		Privilege    int    `json:"privilege"`
	}

	UserSession struct {
		UserId int    `gorm:"column:user_id"`
		Token  string `gorm:"column:token"`
	}
)

type Auth struct {
	Login    string `form:"login"`
	Password string `form:"password"`
}
