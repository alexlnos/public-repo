package entity

type OrderProduct struct {
	Id        int `gorm:"column:id"`
	OrderId   int `gorm:"column:order_id"`
	ProductId int `gorm:"column:product_id"`
}
