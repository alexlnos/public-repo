package entity

import (
	"encoding/json"
	"fmt"
	"reflect"
)

// MarshalField
func MarshalField(src interface{}) ([]byte, error) {
	return json.Marshal(src)
}

// UnmarshalField
func UnmarshalField(src interface{}, out interface{}) error {
	switch src := src.(type) {
	case nil:
		return nil

	case string:
		// empty descriptor
		if src == "" {
			return nil
		}

		err := json.Unmarshal([]byte(src), out)
		if err != nil {
			return fmt.Errorf("Json field: %v", err)
		}

	case []byte:
		// empty descriptor
		if len(src) == 0 {
			return nil
		}

		err := json.Unmarshal(src, out)
		if err != nil {
			return fmt.Errorf("Json field: %v", err)
		}

	default:
		return fmt.Errorf("Json field: unable to unmarshall type %T", src)
	}

	return nil
}

// Copy скопирует любую сущность.
func Copy(e interface{}) interface{} {
	return reflect.New(reflect.ValueOf(e).Elem().Type()).Interface()
}
