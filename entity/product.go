package entity

type Product struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	ProjectId int    `json:"-"`
	Slug      string `json:"slug"`
}

type ProductFilter struct {
	ProjectId int `form:"project_id"`
}
