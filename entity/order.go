package entity

const (
	MessengerTypeInstagram = iota + 1
	MessengerTypeWhatsApp
)

const (
	DeliveryTypeRussianPost = iota + 1
	DeliveryTypeCdek
	DeliveryTypePickup
)

const (
	OrderTypeClient = iota + 1
	OrderTypeBarter
	OrderTypeDefect
)

type Order struct {
	Id int `gorm:"column:id" json:"id"`

	Type      int     `gorm:"column:type" json:"type"`
	TotalCost float64 `gorm:"column:total_cost" json:"total_cost"`

	UserId   int `gorm:"column:user_id" json:"user_id"`
	ClientId int `gorm:"column:client_id" json:"client_id"`

	PrepayType int     `gorm:"column:prepay_type" json:"prepay_type"`
	PrepayCost float64 `gorm:"column:prepay_cost" json:"prepay_cost"`

	DeliveryType int     `gorm:"column:delivery_type" json:"delivery_type"`
	DeliveryCost float64 `gorm:"column:delivery_cost" json:"delivery_cost"`

	TrackCode string `gorm:"column:track_code" json:"track_code"`
	TrackAuto bool   `gorm:"column:track_auto" json:"track_auto"`

	Comment string `gorm:"column:comment" json:"comment"`
}

type OrderPayload struct {
	Client struct {
		Id       int    `json:"id"`
		FullName string `json:"full_name"`
		Address  string `json:"address"`
		Phone    string `json:"phone"`

		Messengers []struct {
			Type  int    `json:"type"`
			Value string `json:"value"`
		} `json:"messengers"`
	} `json:"client"`

	Order struct {
		TotalCost float64 `json:"total_cost"`
		Type      int     `json:"type"`
		Comment   string  `json:"comment"`

		Prepay struct {
			Type int     `json:"type"`
			Cost float64 `json:"cost"`
		} `json:"prepay"`

		Delivery struct {
			Type      int     `json:"type"`
			Cost      float64 `json:"cost"`
			TrackCode string  `json:"track_code"`
			TrackAuto bool    `json:"track_auto"`
		} `json:"delivery"`
	} `json:"order"`

	ProductIds []int `json:"product_ids"`
}

type OrderListResponse struct {
	Order        Order        `json:"order"`
	Client       Client       `json:"client"`
	ShippingInfo ShippingInfo `json:"shipping_info"`
	Product      []Product    `json:"product"`
}

type OrderListFilter struct {
	Id           []int   `form:"id"`
	OrderType    int     `form:"order_type"`
	ShippingType int     `form:"shipping_type"`
	DeliveryType []int   `form:"delivery_type"`
	ProjectId    int     `form:"project_id"`
	TrackCode    *string `form:"track_code"`
}
