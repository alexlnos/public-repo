CREATE TABLE projects
(
  id         SERIAL NOT NULL CONSTRAINT projects_pkey PRIMARY KEY,
  name       VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TRIGGER
  projects_updated_at_now
  BEFORE UPDATE
  ON
    projects
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();