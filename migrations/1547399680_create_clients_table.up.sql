CREATE TABLE clients
(
  id         SERIAL NOT NULL CONSTRAINT clients_pkey PRIMARY KEY,
  full_name  VARCHAR(255),
  address    TEXT,
  phone      VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TRIGGER
  clients_updated_at_now
  BEFORE UPDATE
  ON
    clients
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();