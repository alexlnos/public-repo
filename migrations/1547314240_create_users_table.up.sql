CREATE TABLE users
(
  id            SERIAL NOT NULL CONSTRAINT users_pkey PRIMARY KEY,
  name          VARCHAR(255),
  login         VARCHAR(255),
  password_hash VARCHAR(255),
  privilege     SMALLINT,
  created_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at    TIMESTAMP
);

CREATE UNIQUE INDEX users_login_unique_idx
  ON users (login);

CREATE TRIGGER
  users_updated_at_now
  BEFORE UPDATE
  ON
    users
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();