CREATE TABLE order_products
(
  id         SERIAL NOT NULL CONSTRAINT order_products_pkey PRIMARY KEY,
  order_id   INTEGER,
  product_id INTEGER,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TRIGGER
  order_products_updated_at_now
  BEFORE UPDATE
  ON
    order_products
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();