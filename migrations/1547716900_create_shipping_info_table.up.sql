CREATE TABLE shipping_info
(
  id         SERIAL NOT NULL CONSTRAINT shipping_info_pkey PRIMARY KEY,
  date       TIMESTAMP,
  order_id   INTEGER,
  status     INTEGER,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE UNIQUE INDEX shipping_info_status_unique_idx
  ON shipping_info (order_id, status);

CREATE TRIGGER
  shipping_info_updated_at_now
  BEFORE UPDATE
  ON
    shipping_info
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();