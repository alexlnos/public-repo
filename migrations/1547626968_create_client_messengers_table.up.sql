CREATE TABLE client_messengers
(
  id         SERIAL NOT NULL CONSTRAINT client_messengers_pkey PRIMARY KEY,
  client_id  INTEGER,
  type       INTEGER,
  value      VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TRIGGER
  client_messengers_updated_at_now
  BEFORE UPDATE
  ON
    client_messengers
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();