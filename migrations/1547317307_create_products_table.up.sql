CREATE TABLE products
(
  id         SERIAL NOT NULL CONSTRAINT products_pkey PRIMARY KEY,
  name       VARCHAR(255),
  project_id INTEGER,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE INDEX products_project_id_idx
  ON products (project_id);

CREATE TRIGGER
  products_updated_at_now
  BEFORE UPDATE
  ON
    products
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();