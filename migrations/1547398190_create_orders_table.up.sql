CREATE TABLE orders
(
  id            SERIAL NOT NULL CONSTRAINT orders_pkey PRIMARY KEY,
  user_id       INTEGER,
  client_id     INTEGER,
  type          INTEGER,
  total_cost    FLOAT,
  prepay_type   INTEGER,
  prepay_cost   FLOAT,
  delivery_type INTEGER,
  delivery_cost FLOAT,
  track_code    VARCHAR(255),
  track_auto    BOOLEAN,
  comment       TEXT,
  created_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at    TIMESTAMP
);

CREATE TRIGGER
  orders_updated_at_now
  BEFORE UPDATE
  ON
    orders
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();