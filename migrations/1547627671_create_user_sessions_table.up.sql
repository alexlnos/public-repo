CREATE TABLE user_sessions
(
  id         SERIAL NOT NULL CONSTRAINT user_sessions_pkey PRIMARY KEY,
  user_id    INTEGER,
  token      VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE UNIQUE INDEX user_sessions_token_unique_idx
  ON user_sessions (token);

CREATE TRIGGER
  user_sessions_updated_at_now
  BEFORE UPDATE
  ON
    user_sessions
  FOR EACH ROW EXECUTE PROCEDURE
  updated_at_now();