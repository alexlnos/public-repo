package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"panda-crm/entity"
	"panda-crm/middleware/contxt"
	"panda-crm/repository"
	"panda-crm/usecase"
)

func Authorization(ctx *gin.Context) {
	var request = ctx.MustGet(contxt.EntityBind).(*entity.Auth)

	user := entity.User{Login: request.Login, PasswordHash: usecase.Sha256(request.Password)}

	err := repository.GetUserByPassword(&user)
	if err != nil {
		AbortWith(ctx, errors.New("bad credentials"))
		return
	}

	user.Token = uuid.New().String()
	userSession := entity.UserSession{Token: user.Token, UserId: user.Id}

	err = repository.Save(&userSession)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	SetPayload(ctx, user)
	ctx.Next()
}

func VerifyToken(ctx *gin.Context) {
	var user = entity.User{}

	user.Token, _ = ctx.GetQuery("token")

	err := repository.GetUserByToken(&user)
	if err != nil {
		AbortWith(ctx, errors.New("bad token"))
		return
	}

	ctx.Set(contxt.User, &user)
	ctx.Next()
}
