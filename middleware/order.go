package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"panda-crm/entity"
	"panda-crm/middleware/contxt"
	"panda-crm/repository"
	"panda-crm/usecase"
)

func OrderCreate(ctx *gin.Context) {
	var (
		user    = ctx.MustGet(contxt.User).(*entity.User)
		request = ctx.MustGet(contxt.EntityBind).(*entity.OrderPayload)
		client  = ctx.MustGet(contxt.Client).(*entity.Client)
		err     error
	)

	order := entity.Order{
		UserId:   user.Id,
		ClientId: client.Id,

		Type:      request.Order.Type,
		TotalCost: request.Order.TotalCost,

		PrepayType: request.Order.Prepay.Type,
		PrepayCost: request.Order.Prepay.Cost,

		DeliveryType: request.Order.Delivery.Type,
		DeliveryCost: request.Order.Delivery.Cost,

		TrackCode: request.Order.Delivery.TrackCode,
		TrackAuto: request.Order.Delivery.TrackAuto,

		Comment: request.Order.Comment,
	}

	err = repository.Save(&order)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	SetPayload(ctx, order)

	ctx.Set(contxt.Order, &order)
	ctx.Next()
}

func OrderTypeList(ctx *gin.Context) {
	SetPayload(ctx, []entity.Label{
		{
			Id:   entity.OrderTypeClient,
			Name: "Заказ от клиента",
		},
		{
			Id:   entity.OrderTypeBarter,
			Name: "Бартер",
		},
		{
			Id:   entity.OrderTypeDefect,
			Name: "Замена",
		},
	})

	ctx.Next()
}

func OrderUpdate(ctx *gin.Context) {
	var order = ctx.MustGet(contxt.EntityBind).(*entity.Order)

	err := repository.UpdateOrder(order)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	SetPayload(ctx, order)
	ctx.Next()
}

func OrderList(ctx *gin.Context) {
	var filter = ctx.MustGet(contxt.EntityBind).(*entity.OrderListFilter)

	// Orders
	orders, err := repository.GetOrderList(filter)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	orderIds := make([]int, len(orders))
	clientIds := make([]int, len(orders))

	for key, order := range orders {
		orderIds[key] = order.Id
		clientIds[key] = order.ClientId
	}

	// Clients
	clients, err := repository.GetClientByIds(clientIds)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	clientsMap := map[int]entity.Client{}
	for _, client := range clients {
		client.Phone = usecase.FormatPhoneNumber(client.Phone)
		clientsMap[client.Id] = client
	}

	// Messengers
	messengers, err := repository.GetClientMessengerByIds(clientIds)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
	}

	for _, messenger := range messengers {
		client := clientsMap[messenger.ClientId]
		client.Messengers = append(client.Messengers, messenger)
		clientsMap[messenger.ClientId] = client
	}

	// Order Products
	orderProducts, err := repository.GetOrderProductsByOrderIds(orderIds)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	orderProductsMap := map[int][]entity.OrderProduct{}
	for _, orderProduct := range orderProducts {
		orderProductsMap[orderProduct.OrderId] = append(orderProductsMap[orderProduct.OrderId], orderProduct)
	}

	// Products
	productList, err := repository.GetProductList(&entity.Project{})
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	productsMap := map[int]entity.Product{}
	for _, product := range productList {
		productsMap[product.Id] = product
	}

	// Shipping Info
	shippingInfoList, err := repository.GetShippingInfoByOrderIds(orderIds)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	shippingInfoMap := map[int]entity.ShippingInfo{}
	for _, shippingInfo := range shippingInfoList {
		shippingInfoMap[shippingInfo.OrderId] = shippingInfo
	}

	// Result
	response := make([]entity.OrderListResponse, len(orders))

	for key, order := range orders {
		products := make([]entity.Product, len(orderProductsMap[order.Id]))

		for key, orderProduct := range orderProductsMap[order.Id] {
			products[key] = productsMap[orderProduct.ProductId]
		}

		response[key].Order = order
		response[key].Client = clientsMap[order.ClientId]
		response[key].ShippingInfo = shippingInfoMap[order.Id]
		response[key].Product = products
	}

	SetPayload(ctx, response)
	ctx.Next()
}

func OrderValidation(ctx *gin.Context) {
	var request = ctx.MustGet(contxt.EntityBind).(*entity.OrderPayload)

	if request.Client.Id == 0 {
		if request.Client.FullName == "" {
			AbortWith(ctx, errors.New("full name empty"))
			return
		}

		if request.Client.Address == "" {
			AbortWith(ctx, errors.New("address empty"))
			return
		}

		if request.Client.Phone == "" {
			AbortWith(ctx, errors.New("phone empty"))
			return
		}
	}

	if len(request.ProductIds) == 0 {
		AbortWith(ctx, errors.New("product ids = 0"))
		return
	}

	ctx.Next()
}
