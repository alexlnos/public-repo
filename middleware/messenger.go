package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"panda-crm/entity"
	"panda-crm/middleware/contxt"
	"panda-crm/repository"
)

func ClientMessengerCreate(ctx *gin.Context) {
	var (
		request = ctx.MustGet(contxt.EntityBind).(*entity.OrderPayload)
		client  = ctx.MustGet(contxt.Client).(*entity.Client)
	)

	for _, messenger := range request.Client.Messengers {
		err := repository.Create(&entity.ClientMessenger{ClientId: client.Id, Type: messenger.Type, Value: messenger.Value})
		if err != nil {
			AbortWith(ctx, errors.New("internal error"))
			return
		}
	}

	ctx.Next()
}

func MessengerList(ctx *gin.Context) {
	SetPayload(ctx, []entity.Label{
		{
			Id:   entity.MessengerTypeInstagram,
			Name: "Instagram",
		},
		{
			Id:   entity.MessengerTypeWhatsApp,
			Name: "WhatsApp",
		},
	})

	ctx.Next()
}
