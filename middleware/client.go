package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"panda-crm/entity"
	"panda-crm/middleware/contxt"
	"panda-crm/repository"
)

func ClientCreate(ctx *gin.Context) {
	var request = ctx.MustGet(contxt.EntityBind).(*entity.OrderPayload)

	// Client
	client := entity.Client{
		FullName: request.Client.FullName,
		Address:  request.Client.Address,
		Phone:    request.Client.Phone,
	}

	err := repository.Save(&client)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	ctx.Set(contxt.Client, &client)
	ctx.Next()
}
