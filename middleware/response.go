package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"panda-crm/middleware/contxt"
)

const StatusOk = "OK"

// EndpointResponse
type (
	Context struct {
		Meta    *Meta
		Payload interface{}
		Error   error
	}

	EndpointResponse struct {
		Meta     *Meta       `json:"meta,omitempty"`
		Response interface{} `json:"response"`
	}

	EndpointError struct {
		Error string `json:"error"`
	}

	Meta struct {
		IsLastPage bool `json:"is_last_page"`
		TotalCount int  `json:"total"`
	}
)

// Result to get current context result.
func Result(ctx *gin.Context) *Context {
	return ctx.MustGet(contxt.Result).(*Context)
}

// Error to check for error existence in context.
func Error(ctx *gin.Context) error {
	return Result(ctx).Error
}

func SetPayload(ctx *gin.Context, value interface{}) {
	Result(ctx).Payload = value
}

// AbortWith aborts request processing with error.
func AbortWith(ctx *gin.Context, err error) {
	Result(ctx).Error = err
	ctx.Abort()
}

// ResponseWriter provides result structure to context,
// normalize, format and sends response to a client.
func ResponseWriter(ctx *gin.Context) {
	ctx.Set(contxt.Result, &Context{})
	ctx.Next()

	result := Result(ctx)

	if result.Error != nil {
		ctx.JSON(http.StatusNotFound, EndpointError{Error: result.Error.Error()})
	} else {
		ctx.JSON(http.StatusOK, EndpointResponse{Response: result.Payload, Meta: result.Meta})
	}
}
