package middleware

import (
	"github.com/gin-gonic/gin"
	"panda-crm/entity"
)

func DeliveryList(ctx *gin.Context) {
	SetPayload(ctx, []entity.Label{
		{
			Id:   entity.DeliveryTypeRussianPost,
			Name: "Почта России",
		},
		{
			Id:   entity.DeliveryTypeCdek,
			Name: "СДЭК",
		},
		{
			Id:   entity.DeliveryTypePickup,
			Name: "Самовывоз",
		},
	})

	ctx.Next()
}
