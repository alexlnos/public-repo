package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"panda-crm/entity"
	"panda-crm/middleware/contxt"
	"panda-crm/repository"
)

func ProductList(ctx *gin.Context) {
	var request = ctx.MustGet(contxt.EntityBind).(*entity.ProductFilter)

	list, err := repository.GetProductList(&entity.Project{Id: request.ProjectId})
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	SetPayload(ctx, list)
	ctx.Next()
}

func OrderProductCreate(ctx *gin.Context) {
	var (
		request = ctx.MustGet(contxt.EntityBind).(*entity.OrderPayload)
		order   = ctx.MustGet(contxt.Order).(*entity.Order)
	)

	for _, productId := range request.ProductIds {
		err := repository.Create(&entity.OrderProduct{OrderId: order.Id, ProductId: productId})
		if err != nil {
			AbortWith(ctx, errors.New("internal error"))
			return
		}
	}

	ctx.Next()
}
