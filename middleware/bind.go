package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"panda-crm/entity"
	"panda-crm/middleware/contxt"
)

func BindEntity(prototype interface{}) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		data := entity.Copy(prototype)

		if err := ctx.ShouldBind(data); err != nil {
			SetPayload(ctx, err.Error())
			AbortWith(ctx, errors.New("validation failed"))
			return
		}

		ctx.Set(contxt.EntityBind, data)
		ctx.Next()
	}
}
