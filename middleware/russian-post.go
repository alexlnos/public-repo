package middleware

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/unidoc/unidoc/pdf/creator"
	"os"
	"panda-crm/entity"
	"panda-crm/middleware/contxt"
	"panda-crm/repository"
	"panda-crm/usecase"
)

const blankPath = "temporary/blanks.pdf"

func DownloadOrderBlank(ctx *gin.Context) {
	var request = ctx.MustGet(contxt.EntityBind).(*entity.OrderBlankPayload)

	// Orders
	orders, err := repository.GetOrderList(&entity.OrderListFilter{Id: request.Id})
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	ordersMap := map[int]entity.Order{}
	for _, order := range orders {
		ordersMap[order.Id] = order
	}

	// Products
	productList, err := repository.GetProductList(&entity.Project{})
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	productsMap := map[int]entity.Product{}
	for _, product := range productList {
		productsMap[product.Id] = product
	}

	// Order products
	orderProducts, err := repository.GetOrderProductsByOrderIds(request.Id)
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	orderProductsMap := map[int][]entity.Product{}
	for _, orderProduct := range orderProducts {
		orderProductsMap[orderProduct.OrderId] = append(orderProductsMap[orderProduct.OrderId], productsMap[orderProduct.ProductId])
	}

	blanks := make([]entity.RussianPostBlank, 0, len(request.Id))

	pdfFile := creator.New()

	for orderId, products := range orderProductsMap {
		blank := entity.RussianPostBlank{TrackCode: ordersMap[orderId].TrackCode}

		err := repository.DownloadRussianPostBlank(&blank)
		defer os.Remove(blank.Path)

		if err != nil {
			AbortWith(ctx, errors.New(fmt.Sprintf("track code %s contains error", blank.TrackCode)))
			return
		}

		err = repository.AddOrderInformation(pdfFile, blank, ordersMap[orderId], products)
		if err != nil {
			AbortWith(ctx, err)
			return
		}

		blanks = append(blanks, blank)
	}

	pdfFile.WriteToFile(blankPath)
	defer os.Remove(blankPath)

	ctx.Header("Content-Description", "File Transfer")
	ctx.Header("Content-Transfer-Encoding", "binary")
	ctx.Header("Content-OrderType", "application/octet-stream")
	ctx.Header("Content-Disposition", fmt.Sprintf("attachment; filename=blanks-%s.pdf", usecase.JoinNumbers(request.Id, "-")))
	ctx.File(blankPath)
	ctx.Abort()
}
