package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"panda-crm/repository"
)

func ProjectList(ctx *gin.Context) {
	list, err := repository.GetProjectList()
	if err != nil {
		AbortWith(ctx, errors.New("internal error"))
		return
	}

	SetPayload(ctx, list)
	ctx.Next()
}
