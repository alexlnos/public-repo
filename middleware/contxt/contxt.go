package contxt

const (
	Entity     = "Entity"
	EntityBind = "EntityBind"
	Client     = "Client"
	Order      = "Order"
	Result     = "Result"
	View       = "View"
	User       = "User"
	FetchInfo  = "FetchInfo"
	QueryData  = "QueryData"
	BodyData   = "BodyData"
)
