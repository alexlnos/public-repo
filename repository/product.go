package repository

import "panda-crm/entity"

func GetProductList(project *entity.Project) (list []entity.Product, err error) {
	query := db

	if project.Id != 0 {
		query = query.Where("project_id = ?", project.Id)
	}

	err = query.Order("id ASC").Find(&list).Error
	return
}

func GetOrderProductsByOrderIds(ids []int) (list []entity.OrderProduct, err error) {
	err = db.Where("order_id in (?)", ids).Find(&list).Error
	return
}
