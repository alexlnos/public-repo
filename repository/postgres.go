package repository

import (
	"fmt"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/jinzhu/gorm"
	"log"
	"os"
	"time"
)

var (
	db  *gorm.DB
	err error
)

const (
	ERecordNotFound = "record not found"
)

func init() {
	for {
		db, err = gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
			os.Getenv("POSTGRES_HOST"),
			os.Getenv("POSTGRES_PORT"),
			os.Getenv("POSTGRES_USER"),
			os.Getenv("POSTGRES_PASSWORD"),
			os.Getenv("POSTGRES_DB_NAME"),
			os.Getenv("POSTGRES_SSL_MODE"),
		))

		if err != nil {
			time.Sleep(time.Second * 5)
			log.Println(err)
			continue
		}

		break
	}

	initMigration()
}

func initMigration() {
	driver, err := postgres.WithInstance(db.DB(), &postgres.Config{})
	if err != nil {
		log.Println(err)
		return
	}

	migration, err := migrate.NewWithDatabaseInstance("file://migrations", "postgres", driver)
	if err != nil {
		log.Println(err)
		return
	}

	migration.Up()
}

func Create(value interface{}) error {
	return db.Create(value).Error
}

func Save(value interface{}) error {
	return db.Save(value).Error
}
