package repository

import (
	"errors"
	"fmt"
	"gopkg.in/resty.v1"
	"panda-crm/entity"
)

func GetRussianPostTrackInfo(trackCode string) (trackInfo entity.RussianPostTrackPayload, err error) {
	var resp *resty.Response

	resp, err = resty.
		R().
		SetResult(&trackInfo).
		Get(fmt.Sprintf("https://www.pochta.ru/tracking?p_p_id=trackingPortlet_WAR_portalportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=getList&p_p_cacheability=cacheLevelPage&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&barcodeList=%s", trackCode))

	if err != nil {
		return
	}

	if resp.StatusCode() != 200 {
		err = errors.New("status code != 200")
		return
	}

	return
}

func DownloadRussianPostBlank(blank *entity.RussianPostBlank) error {
	var resp *resty.Response

	blank.Path = fmt.Sprintf("temporary/blank-%s.pdf", blank.TrackCode)

	resp, err = resty.R().
		SetHeader("Referer", "https://www.pochta.ru/").
		SetOutput(blank.Path).
		Get(fmt.Sprintf("https://www.pochta.ru/portal-pdf-form/restful/blank/%s/main", blank.TrackCode))

	if err != nil {
		return err
	}

	if resp.StatusCode() != 200 {
		return errors.New("status code != 200")
	}

	return nil
}
