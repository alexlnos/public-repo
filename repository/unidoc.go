package repository

import (
	"fmt"
	"github.com/unidoc/unidoc/pdf/creator"
	pdf "github.com/unidoc/unidoc/pdf/model"
	"github.com/unidoc/unidoc/pdf/model/fonts"
	"os"
	"panda-crm/entity"
	"strconv"
	"strings"
)

func AddOrderInformation(c *creator.Creator, blank entity.RussianPostBlank, order entity.Order, products []entity.Product) error {
	f, err := os.Open(blank.Path)
	if err != nil {
		return err
	}
	defer f.Close()

	pdfReader, err := pdf.NewPdfReader(f)
	if err != nil {
		return err
	}

	numPages, err := pdfReader.GetNumPages()
	if err != nil {
		return err
	}

	// Load the pages.
	for i := 0; i < numPages; i++ {
		page, err := pdfReader.GetPage(i + 1)
		if err != nil {
			return err
		}

		err = c.AddPage(page)
		if err != nil {
			return err
		}

		if i == 0 {
			// Order ID
			p := creator.NewParagraph(strconv.Itoa(order.Id))
			p.SetColor(creator.ColorBlack)
			p.SetFontSize(20)
			p.SetFont(fonts.NewFontHelveticaBold())
			p.SetPos(440, 17)
			_ = c.Draw(p)

			// Order info
			p = creator.NewParagraph("Order information:")
			p.SetColor(creator.ColorBlack)
			p.SetFontSize(20)
			p.SetFont(fonts.NewFontHelveticaBold())
			p.SetPos(20, 430)
			_ = c.Draw(p)

			var productsCount []string
			for _, product := range products {
				productsCount = append(productsCount, fmt.Sprintf("1 %s", product.Slug))
			}

			p = creator.NewParagraph(strings.Join(productsCount, ", "))
			p.SetColor(creator.ColorBlack)
			p.SetFontSize(16)
			p.SetFont(fonts.NewFontHelvetica())
			p.SetPos(20, 455)
			_ = c.Draw(p)
		}
	}

	return nil
}
