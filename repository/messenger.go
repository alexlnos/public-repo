package repository

import "panda-crm/entity"

func GetClientMessengerByIds(ids []int) (list []entity.ClientMessenger, err error) {
	err = db.Where("client_id in (?)", ids).Find(&list).Error
	return
}
