package repository

import "panda-crm/entity"

func GetUserByPassword(user *entity.User) error {
	return db.Where("login = ? and password_hash = ?", user.Login, user.PasswordHash).Find(&user).Error
}

func GetUserByToken(user *entity.User) error {
	return db.Select("users.*").Table("user_sessions").Joins("LEFT JOIN users ON user_sessions.user_id = users.id").Where("user_sessions.token = ?", user.Token).First(&user).Error
}
