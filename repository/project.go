package repository

import "panda-crm/entity"

func GetProjectList() (list []entity.Project, err error) {
	err = db.Order("id ASC").Find(&list).Error
	return
}
