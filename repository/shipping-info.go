package repository

import (
	"fmt"
	"panda-crm/entity"
	"strings"
)

func GetShippingInfoByOrderIds(ids []int) (list []entity.ShippingInfo, err error) {
	err = db.Raw("SELECT * FROM shipping_info WHERE id in (SELECT max(id) FROM shipping_info WHERE order_id in (?) GROUP BY order_id)", ids).Find(&list).Error
	return
}

func SaveShippingInfo(list []entity.ShippingInfo) error {
	if len(list) == 0 {
		return nil
	}

	valueStrings := []string{}
	valueArgs := []interface{}{}

	for _, element := range list {
		valueStrings = append(valueStrings, "(?, ?, ?)")
		valueArgs = append(valueArgs, element.Date)
		valueArgs = append(valueArgs, element.OrderId)
		valueArgs = append(valueArgs, element.Status)
	}

	smt := `INSERT INTO shipping_info(date, order_id, status) VALUES %s ON CONFLICT DO NOTHING`
	smt = fmt.Sprintf(smt, strings.Join(valueStrings, ","))

	return db.Exec(smt, valueArgs...).Error
}
