package repository

import "panda-crm/entity"

func GetClientByIds(ids []int) (clients []entity.Client, err error) {
	err = db.Where("id in (?)", ids).Find(&clients).Error
	return
}
