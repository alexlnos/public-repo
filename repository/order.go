package repository

import "panda-crm/entity"

func GetRussianPostOrders() (list []entity.Order, err error) {
	err = db.Where("delivery_type = ? and track_code != ''", entity.DeliveryTypeRussianPost).Find(&list).Error
	return
}

func GetOrderList(filter *entity.OrderListFilter) (orders []entity.Order, err error) {
	query := db.Table("orders")

	if len(filter.Id) > 0 {
		query = query.Where("orders.id in (?)", filter.Id)
	}

	if filter.ShippingType != 0 {
		query = query.Where("orders.id in (SELECT order_id as id FROM (SELECT max(status) as shipping_type, order_id FROM shipping_info GROUP BY order_id) as o WHERE o.shipping_type = ?)", filter.ShippingType)
	}

	if filter.OrderType != 0 {
		query = query.Where("orders.type = ?", filter.OrderType)
	}

	if filter.TrackCode != nil {
		query = query.Where("orders.track_code = ?", filter.TrackCode)
	}

	if len(filter.DeliveryType) > 0 {
		query = query.Where("orders.delivery_type in (?)", filter.DeliveryType)
	}

	if filter.ProjectId != 0 {
		query = query.Where("orders.id in (SELECT order_id FROM order_products WHERE product_id in (SELECT id FROM products WHERE project_id = ?) GROUP BY order_id)", filter.ProjectId)
	}

	err = query.Order("id DESC").Find(&orders).Error
	return
}

func UpdateOrder(order *entity.Order) error {
	fields := map[string]interface{}{}

	if order.Comment != "" {
		fields["comment"] = order.Comment
	}

	if order.TrackCode != "" {
		fields["track_code"] = order.TrackCode
	}

	if order.DeliveryCost != 0 {
		fields["delivery_cost"] = order.DeliveryCost
	}

	err = db.Debug().Model(&entity.Order{}).Where("id = ?", order.Id).Updates(fields).Error
	if err != nil {
		return err
	}

	err := db.Debug().First(&order).Error
	if err != nil {
		return err
	}

	return nil
}
