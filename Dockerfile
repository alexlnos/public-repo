FROM golang:1.11.4-alpine

COPY . /go/src/panda-crm
WORKDIR /go/src/panda-crm

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh && \
    go get ./... && \
    go install .

CMD ["panda-crm"]

EXPOSE 80 443