package usecase

import (
	"strconv"
	"strings"
)

func JoinNumbers(nums []int, sep string) string {
	var str []string

	for _, num := range nums {
		str = append(str, strconv.Itoa(num))
	}

	return strings.Join(str, sep)
}
