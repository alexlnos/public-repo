package usecase

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
)

func Sha256(value string) string {
	h256 := sha256.New()
	io.WriteString(h256, value)
	return hex.EncodeToString(h256.Sum(nil))
}
