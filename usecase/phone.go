package usecase

import "github.com/ttacon/libphonenumber"

func FormatPhoneNumber(val string) string {
	num, _ := libphonenumber.Parse(val, "RU")
	return libphonenumber.Format(num, libphonenumber.E164)
}
